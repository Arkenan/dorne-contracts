require_relative '../errors/unknown_property_error'
require_relative 'properties/client'
require_relative 'properties/cost'
require_relative 'properties/repetitions'
require_relative 'properties/content'
require_relative 'properties/signals'
require_relative 'properties/frequency'

class Ammendment
  PROPERTIES = {
    client: Client,
    cost: Cost,
    content: Content,
    repetitions: Repetitions,
    frequency: Frequency,
    signals: Signals
  }.freeze

  def initialize(data)
    data.keys.each do |key|
      raise UnknownPropertyError unless PROPERTIES.include? key
    end
    @data = data
  end

  def apply_to(contract)
    @data.each do |key, value|
      PROPERTIES[key].new(value).apply_to(contract)
    end
  end
end
