require_relative 'property'

class Repetitions < Property
  def apply_to(contract)
    contract.repetitions = value
  end
end
