require_relative 'property'

class Cost < Property
  def apply_to(contract)
    contract.cost = value
  end
end
