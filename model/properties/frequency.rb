require_relative 'property'

class Frequency < Property
  def apply_to(contract)
    contract.frequency = value
  end
end
