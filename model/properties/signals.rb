require_relative 'property'

class Signals < Property
  def apply_to(contract)
    contract.signals = value
  end
end
