require_relative 'property'

class Content < Property
  def apply_to(contract)
    contract.content = value
  end
end
