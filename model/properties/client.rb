require_relative 'property'

class Client < Property
  def apply_to(contract)
    contract.client = value
  end
end
