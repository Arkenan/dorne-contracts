require_relative '../errors/contract_confirmed_error'
require_relative '../errors/contract_not_confirmed_error'

module ConfirmationChecker
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def check_confirmed_to_write(*attrs)
      attrs.each do |attr|
        puts attr
        puts "#{attr}="
        define_method "#{attr}=" do |attr_arg|
          raise ContractConfirmedError if @confirmed

          instance_variable_set("@#{attr}", attr_arg)
        end
      end
    end
  end
end

class Contract
  include ConfirmationChecker
  attr_reader :start_date, :client, :cost, :content, :repetitions,
              :frequency, :signals

  check_confirmed_to_write :start_date, :client, :cost, :content, :repetitions,
                           :frequency, :signals

  # rubocop:disable Metrics/ParameterLists
  def initialize(start_date, client, cost, content, repetitions, frequency,
                 signals)
    # rubocop:enable Metrics/ParameterLists

    @start_date = start_date
    @client = client
    @cost = cost
    @content = content
    @repetitions = repetitions
    @frequency = frequency
    @signals = signals

    @confirmed = false
    @ammendments = []
  end

  def self.from_contract(contract)
    Contract.new(contract.start_date, contract.client, contract.cost,
                 contract.content, contract.repetitions, contract.frequency,
                 contract.signals)
  end

  def confirm
    @confirmed = true
  end

  def ammend(ammendment)
    raise ContractNotConfirmedError unless @confirmed

    @ammendments << ammendment
  end

  def at_ammendment(index)
    contract = Contract.from_contract(self)

    @ammendments[0..index - 1].each do |ammendment|
      ammendment.apply_to(contract)
    end

    contract
  end

  def current
    at_ammendment(@ammendments.length)
  end
end
