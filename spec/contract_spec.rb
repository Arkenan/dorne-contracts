require 'rspec'
require 'date'
require_relative '../errors/contract_confirmed_error'
require_relative '../errors/contract_not_confirmed_error'
require_relative '../model/ammendment'
require_relative '../model/contract'

describe Contract do
  let(:contract) do
    described_class.new(Date.parse('20190101'), 'artear', 10_000,
                        %w['Volver al futuro'], 1, 1, %w[volver canal13])
  end

  describe 'model' do
    it { expect(contract).to respond_to(:start_date) }
    it { expect(contract).to respond_to(:client) }
    it { expect(contract).to respond_to(:cost) }
    it { expect(contract).to respond_to(:content) }
    it { expect(contract).to respond_to(:repetitions) }
    it { expect(contract).to respond_to(:frequency) }
    it { expect(contract).to respond_to(:signals) }
  end

  describe 'confirmation and contract modification' do
    it 'Changing the cost to 20000 should be allowed before confirming.' do
      contract.cost = 20_000
      expect(contract.cost).to eq 20_000
    end

    it 'Changing the cost to 20000 should not be allowed after confirming' do
      contract.confirm
      expect { contract.cost = 20_000 }.to raise_error(ContractConfirmedError)
    end
  end

  describe 'confirmation and ammendments' do
    let(:cost_ammendment) { Ammendment.new(cost: 30_000) }

    it 'A confirmed contract should be ammendable with a cost ammendment' do
      contract.confirm
      contract.ammend(cost_ammendment)
      expect(contract.cost).to eq 10_000
      expect(contract.at_ammendment(1).cost).to eq 30_000
    end

    it 'A non confirmed contract can not be ammended' do
      expect { contract.ammend(cost_ammendment) }.to raise_error(ContractNotConfirmedError)
    end
  end

  describe 'current contract' do
    let(:cost_ammendment1) { Ammendment.new(cost: 30_000) }
    let(:cost_ammendment2) { Ammendment.new(cost: 40_000) }

    it 'After one ammendment, the current contract costs 30_000' do
      contract.confirm
      contract.ammend(cost_ammendment1)
      expect(contract.current.cost).to eq 30_000
    end

    it 'After the two ammendments, the current contract costs 40_000' do
      contract.confirm
      contract.ammend(cost_ammendment1)
      contract.ammend(cost_ammendment2)
      expect(contract.current.cost).to eq 40_000
    end
  end

  describe 'multiple ammendments' do
    let(:multiple_ammendment) { Ammendment.new(cost: 50, repetitions: 7) }
    let(:cost_ammendment) { Ammendment.new(cost: 50) }
    let(:repetitions_ammendment) { Ammendment.new(repetitions: 7) }

    it 'One ammendment with multiple fields' do
      contract.confirm
      contract.ammend(multiple_ammendment)
      expect(contract.current.cost).to eq(50)
      expect(contract.current.repetitions).to eq(7)
    end

    it 'Contract with a separate cost and repetitions ammendments' do
      contract.confirm
      contract.ammend(cost_ammendment)
      contract.ammend(repetitions_ammendment)
      expect(contract.current.cost).to eq(50)
      expect(contract.current.repetitions).to eq(7)
    end

    it 'intermediate states' do
      contract.confirm
      contract.ammend(cost_ammendment)
      contract.ammend(repetitions_ammendment)
      expect(contract.at_ammendment(1).repetitions).to eq(1)
      expect(contract.at_ammendment(2).repetitions).to eq(7)
    end
  end
end
