require 'rspec'
require 'date'

require_relative '../model/ammendment'
require_relative '../model/contract'
require_relative '../errors/unknown_property_error'

describe Ammendment do
  let(:contract) do
    Contract.new(Date.parse('20190101'), 'artear', 10_000,
                 %w['Volver al futuro'], 1, 1, %w[volver canal13])
  end

  let(:cost_ammendment) { described_class.new(cost: 30_000) }
  let(:client_ammendment) { described_class.new(client: 'cinemark') }
  let(:repetitions_ammendment) { described_class.new(repetitions: 3) }
  let(:content_ammendment) { described_class.new(content: 'pokemon') }
  let(:signals_ammendment) { described_class.new(signals: %w[cinecanal]) }

  it 'ammend a contract with cost' do
    cost_ammendment.apply_to(contract)
    expect(contract.cost).to eq 30_000
  end

  it 'ammend a contract with client' do
    client_ammendment.apply_to(contract)
    expect(contract.client).to eq 'cinemark'
  end

  it 'ammend a contract with repetitions' do
    repetitions_ammendment.apply_to(contract)
    expect(contract.repetitions).to eq 3
  end

  it 'ammend a contract with content' do
    content_ammendment.apply_to(contract)
    expect(contract.content).to eq 'pokemon'
  end

  it 'ammend a contract with signals' do
    signals_ammendment.apply_to(contract)
    expect(contract.signals).to eq %w[cinecanal]
  end

  it 'ammend a contract with two different ammendments' do
    cost_ammendment.apply_to(contract)
    client_ammendment.apply_to(contract)
    expect(contract.client).to eq 'cinemark'
    expect(contract.cost).to eq 30_000
  end

  it 'creating an ammend with an unknown field should give an error' do
    expect { described_class.new(font: 'Roboto') }.to raise_error UnknownPropertyError
  end
end
