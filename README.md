# Dorne Contracts

## Descripción del problema

Una empresa de generación de contenidos celebra contratos para comercializar los contenidos que genera.

Cada contrato define:

* Información general: fecha del firma del contrato, cliente y monto (u$d).
* Contenido: que puede ser uno o varios, pero al menos 1.
* Licencia de uso: cantidad de repeticiones, frecuencia de repetición, señales en las que el contenido puede ser reproducido.

Mientras que el contrato no está confirmado, el mismo puede ser modificado libremente, pero una vez confirmado la únicamente forma de alterar sus propiedaeds es mediante la generación de enmiendas. Un contrato puede recibir N enmiendas, donde cada enmieda puede modificar distintas partes del contrato. Al mismo tiempo las enmiendas son acumulativas, o sea varias enmienda pueden modificar el mismo elemento del contrato.

El contrato tiene entonces un estado original que es el del momento de su confirmación y luego tiene tantos estados como enmiendas, siendo el estado actual, el resultante de aplicar todas las enmiendas en orden.

### Ejemplo 1: contrato sin confirmar se puede modificar

```
contrato1
fecha: 20190101
cliente: artear
monto: 10000
contenido: Volver al futuro
licencia:
repeticiones: 1
frecuencia: 1
señales: volver, canal13
```

```
contrato1
monto: 200000
```

### Ejemplo 2: contrato confirmado no se puede modificar

```
contrato2
fecha: 20190101
cliente: artear
monto: 10000
contenido: Volver al futuro
licencia:
repeticiones: 1
frecuencia: 1
señales: volver, canal13
```

```
confirmar contrato2

contrato2
monto: 200000 => error
```

### Ejemplo 3: contrato sin confirmar no se puede enmendar

```
contrato3
fecha: 20190101
cliente: artear
monto: 10000
contenido: Volver al futuro
licencia:
repeticiones: 1
frecuencia: 1
señales: volver, canal13
```

```
enmendar contrato3 con monto: 200000 => error
```

### Ejemplo 4: contrato con monto enmendado

```
contrato4
fecha: 20190101
cliente: artear
monto: 10000
contenido: Volver al futuro
licencia:
repeticiones: 1
frecuencia: 1
señales: volver, canal13
```

```
confirmar contrato4

enmendar contrato4 con monto: 200000

Entonces el monto del contrato4 actual es 200000
```

### Ejemplo 5: contrato con enmienda de monto y enmienda de repeticiones

```
contrato5
fecha: 20190101
cliente: artear
monto: 100
contenido: Volver al futuro
licencia:
repeticiones: 1
frecuencia: 1
señales: volver, canal13
```

```
confirmar contrato5

enmendar contrato5 con enmienda1 de monto: 50
enmendar contrato5 con enmienda2 de repeticiones: 7

Entonces el monto del contrato5 actual es 50
y monto del contrato5 original es 100
y las repeticiones del contrato5 actual es 7
y las repeticiones del contrato5 con enmienda1 es 1
y las repeticiones del contrato5 con enmienda2 es 7
y las repeticiones del contrato5 orinal es 1
y la frecuencia del contrato5 actual es 1
y la frecuencia del contrato5 original es 1
```
